%% Runges-Kuta 4 eiles
function fAns = rungeKutta(h, epsilon, minT, maxT, t0, x0, y0, equation)
    ti = t0;

    rangeT = maxT - minT;
    
    steps = floor(rangeT / h);
    
    ansT = [];
    ansX = [];
    ansY = [];
    ansT(1) = t0;
    ansX(1) = x0;
    ansY(1) = y0;
    
    F_txy = @(t,x,y) y;
    G_txy = equation;
    
    minPrev = x0;
    maxPrev = x0;
    
    for i = 1:steps
         rungeKuttaInternal(i);
         
         xi = ansX(i+1);
         
        diffMin = minPrev - xi;
        diffMax = maxPrev - xi;
        
        minPrev = min(minPrev, xi);
        maxPrev = max(maxPrev, xi);
        
        deviation = max(abs(diffMin), abs(diffMax));
   
        %konvergavo su epislon
        if deviation < epsilon && ~isnan(deviation)
           break;
        end
    end
    
    fAns = [ansT; ansX; ansY];
    
    function rungeKuttaInternal(i)
        
        k1 = F_txy(ti,ansX(i),ansY(i));
        L1 = G_txy(ti,ansX(i),ansY(i));
        k2 = F_txy(ti+0.5*h,ansX(i)+0.5*h*k1,ansY(i)+0.5*h*L1);
        L2 = G_txy(ti+0.5*h,ansX(i)+0.5*h*k1,ansY(i)+0.5*h*L1);
        k3 = F_txy((ti+0.5*h),(ansX(i)+0.5*h*k2),(ansY(i)+0.5*h*L2));
        L3 = G_txy((ti+0.5*h),(ansX(i)+0.5*h*k2),(ansY(i)+0.5*h*L2));
        k4 = F_txy((ti+h),(ansX(i)+k3*h),(ansY(i)+L3*h));       
        L4 = G_txy((ti+h),(ansX(i)+k3*h),(ansY(i)+L3*h));

        ansX(i+1) = ansX(i) + (1/6)*(k1+2*k2+2*k3+k4)*h;
        ansY(i+1) = ansY(i) + (1/6)*(L1+2*L2+2*L3+L4)*h;

        ti = ti + h;
        ansT(i+1) = ti;
    end
end