%% bashford

function fAns = bashford(h, epsilon, minT, maxT, t0, x0, y0, equation)
    ti = t0;

    rangeT = maxT - minT;

    steps = floor(rangeT / h);

    fRungeKutta = rungeKutta(h, epsilon, minT+h, minT + h*3, t0, x0, y0, equation);
    
    ansT = fRungeKutta(1,:);
    ansX = fRungeKutta(2,:);
    ansY = fRungeKutta(3,:);

    F_txy = @(t,x,y) y;
    G_txy = equation;
    
    for i = 3:steps
        bashfordInternal(i);
        %break;
    end
    
    fAns = [ansT; ansX];
    
    %http://www.cems.uvm.edu/~tlakoba/math337/notes_3.pdf
    function bashfordInternal(i)  
        tt = getTerms(ansT, i);
        tx = getTerms(ansX, i);
        ty = getTerms(ansY, i);
        
        ansX(i+1) = ansX(i) + h/12*(23*F_txy(tt(1), tx(1), ty(1))-16*F_txy(tt(2), tx(2), ty(2))+5*F_txy(tt(3), tx(3), ty(3)));
        ansY(i+1) = ansY(i) + h/12*(23*G_txy(tt(1), tx(1), ty(1))-16*G_txy(tt(2), tx(2), ty(2))+5*G_txy(tt(3), tx(3), ty(3)));
        
        ti = ti + h;
        ansT(i+1) = ansT(i)+h;

    end

    function terms = getTerms(o, i)
        terms = [o(i); o(i-1); o(i-2)];
    end
end