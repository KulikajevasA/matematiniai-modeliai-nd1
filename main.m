clear all;
clc;
close all;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           DUOTA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% L * (d^2 * Q / dt^2) + R * (dQ / dt) + 1 / C * Q = E(t)
% Q'(0) = 0
% Q(0) = 0
% E(t) = 100 cos(10t)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

E_t = @(t) 100 * cos(10*t);

% H = Ohm * s = s^2 / F
L = 1;                  % 1H
R = 40;                 % 40 Ohms
C = 16 * 10^(-4);       % 16 * 10^-4 F

f2 = lygtis(L, R, C);

epsEq = 10^-5;
eps = 10^-5;

t0 = 0; Q0 = 0; QQ0 = 0;   % pradines salygos

h_min = 0.020;
h = 0.015;
t_min = 0;                 % integravimo intervalas
t_max = 100;

solveEuler = false;
solveAdaptiveEuler = false;
solveRungeKutta = false;
solveFehlberg = false;
solveBashford = true;

if solveEuler
    ansEuler = euler(h, epsEq, t_min, t_max, t0, Q0, QQ0, f2);

    figure('Name', 'Oileris paprastas');
    hold on;
    plot(ansEuler(1,:), ansEuler(2,:), 'b*-');

end

if solveAdaptiveEuler
    ansAdaptiveEuler = eulerAdaptive(h, epsEq, t_min, t_max, t0, Q0, QQ0, f2, 2, h_min);

    figure('Name', 'Oileris su Adaptyviu h');
    hold on;
    plot(ansAdaptiveEuler(1,:), ansAdaptiveEuler(2,:), 'ro-');
end

if solveRungeKutta
    ansRungeKutta = rungeKutta(h, epsEq, t_min, t_max, t0, Q0, QQ0, f2);

    figure('Name', 'Runge-Kuta 4 eile ');
    hold on;
    plot(ansRungeKutta(1,:), ansRungeKutta(2,:), 'ro-');
end

if solveFehlberg
    ansFehlberg = fehlberg(h, epsEq, t_min, t_max, t0, Q0, QQ0, f2);
    
    figure('Name', 'Runge-Kuta-Fehlberg ');
    hold on;
    plot(ansFehlberg(1,:), ansFehlberg(2,:), 'ro-');
end

if solveBashford
    ansBashford = bashford(h, eps, t_min, t_max, t0, Q0, QQ0, f2);

    ansBashford(1,:)
    
    figure('Name', 'Adams-Bashfrod ');
    hold on;
    plot(ansBashford(1,:), ansBashford(2,:), 'ro-');
end
