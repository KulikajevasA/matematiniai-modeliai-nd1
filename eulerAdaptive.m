%% Adaptyvus Oilerio metodas
function fAns = eulerAdaptive(h, epsilon, minT, maxT, t0, x0, y0, equation, multiplier, hMin)   
    rangeT = maxT - minT;

    h = h * multiplier; %pradinis
    while h > (hMin * multiplier)
        ti = t0;
        xi = x0;
        yi = y0;
        
        h = h / multiplier;
        steps = floor(rangeT / h);
    
        ansT = [];
        ansX = [];
        
        ansT(1) = t0;
        ansX(2) = x0;

        minPrev = xi;
        maxPrev = xi;
    
        
        steps = floor(rangeT / h);
        
        for i = 1:steps            
            yi = yi + equation(ti, xi, yi) * h;
            xi = xi + yi * h;
            ti = ti + h;

            diffMin = minPrev - xi;
            diffMax = maxPrev - xi;

            minPrev = min(minPrev, xi);
            maxPrev = max(maxPrev, xi);

            deviation = max(abs(diffMin), abs(diffMax));

            ansT(i+1) = ti;
            ansX(i+1) = xi;
            
            if isnan(deviation)
                break;
            end

            %konvergavo su epislon
            if deviation < epsilon && ~isnan(deviation)
                break;
            end
            
            
        end
    end
    
    h
    fAns = [ansT; ansX];
end