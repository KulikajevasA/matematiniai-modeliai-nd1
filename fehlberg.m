%% runge-kutta-fehlberg

function fAns = fehlberg(h, epsilon, minT, maxT, t0, x0, y0, equation)
    ti = t0;

    ansT = [];
    ansX = [];
    ansY = [];
    ansT(1) = t0;
    ansX(1) = x0;
    ansY(1) = y0;

    F_txy = @(t,x,y) y;
    G_txy = equation;

    i = 1;
    
    while ti < maxT
        fehlbergInternal();
    end
    
    fAns = [ansT; ansY];
    h
    function fehlbergInternal() 
        k1 = h*F_txy(ti, ansX(i), ansY(i));                                                       
        L1 = h*G_txy(ti, ansX(i), ansY(i));                                                       
        k2 = h*F_txy(ti+h/4, ansX(i)+k1/4, ansY(i)+k1/4);                                             
        %L2 = h*G_txy(ti+h/4, ansX(i)+k1/4, ansY(i)+k1/4);                                             
        k3 = h*F_txy(ti+3/8*h, ansX(i)+3/32*k1+9/32*k2, ansY(i)+3/32*k1+9/32*k2);                                
        L3 = h*G_txy(ti+3/8*h, ansX(i)+3/32*k1+9/32*k2, ansY(i)+3/32*k1+9/32*k2);                                
        k4 = h*F_txy(ti+12/13*h, ansX(i)+1932/2197*k1-7200/2197*k2+7296/2197*k3, ansY(i)+1932/2197*k1-7200/2197*k2+7296/2197*k3);        
        L4 = h*G_txy(ti+12/13*h, ansX(i)+1932/2197*k1-7200/2197*k2+7296/2197*k3, ansY(i)+1932/2197*k1-7200/2197*k2+7296/2197*k3);        
        k5 = h*F_txy(ti+h, ansX(i)+439/216*k1-8*k2+3680/513*k3-845/4104*k4, ansY(i)+439/216*k1-8*k2+3680/513*k3-845/4104*k4);            
        L5 = h*G_txy(ti+h, ansX(i)+439/216*k1-8*k2+3680/513*k3-845/4104*k4, ansY(i)+439/216*k1-8*k2+3680/513*k3-845/4104*k4);            
        %k6 = h*F_txy(ti+h/2, ansX(i)-8/27*k1+2*k2-3544/2565*k3+1859/4104*k4-11/40*k5, ansY(i)-8/27*k1+2*k2-3544/2565*k3+1859/4104*k4-11/40*k5); 
        L6 = h*G_txy(ti+h/2, ansX(i)-8/27*k1+2*k2-3544/2565*k3+1859/4104*k4-11/40*k5, ansY(i)-8/27*k1+2*k2-3544/2565*k3+1859/4104*k4-11/40*k5); 
    
        k_x1 = ansX(i) + 25*k1/216+1408*k3/2565+2197*k4/4104-k5/5;
        %k_x2 = ansX(i) + 16*k1/135+6656*k3/12825+28561*k4/56430-9*k5/50+2*k6/55;
        
        L_y1 = ansY(i) + 25*L1/216+1408*L3/2565+2197*L4/4104-L5/5;
        L_y2 = ansY(i) + 16*L1/135+6656*L3/12825+28561*L4/56430-9*L5/50+2*L6/55;
 
        L_R = abs(L_y1-L_y2)/h;
        L_delta = 0.84*(epsilon/L_R)^(1/4);
        
        if L_R<=epsilon
            ti = max(min(ti+h, maxT), minT);
            ansX(i+1) = k_x1;
            ansY(i+1) = L_y1;
            ansT(i+1) = ti;
            i = i+1;
            
            h = L_delta*h;
            
        else
            h = L_delta*h;
        end
    end
end