%% Oilerio metodas
function fAns = euler(h, epsilon, minT, maxT, t0, x0, y0, equation)
    ti = t0;
    xi = x0;
    yi = y0;
    
    rangeT = maxT - minT;
    
    steps = floor(rangeT / h);
    
    ansT = [];
    ansX = [];
    ansT(1) = t0;
    ansX(1) = x0;
    
    deviation = Inf;
    
    minPrev = xi;
    maxPrev = xi;
    
    for i = 1:steps   
        yi = yi + equation(ti, xi, yi) * h;
        xi = xi + yi * h;
        ti = ti + h;
        
        diffMin = minPrev - xi;
        diffMax = maxPrev - xi;
        
        minPrev = min(minPrev, xi);
        maxPrev = max(maxPrev, xi);
        
        deviation = max(abs(diffMin), abs(diffMax));
        
        ansT(i+1) = ti;
        ansX(i+1) = xi;
        
        %konvergavo su epislon
        if deviation < epsilon && ~isnan(deviation)
           break;
        end
    end
    
    fAns = [ansT; ansX];
end